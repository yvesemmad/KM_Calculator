package freedev.yed.kmcalculator;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import static freedev.yed.kmcalculator.Commons.Settings.EssenceMalo;

public class MainActivity extends AppCompatActivity {
    // Variables Globales
    private EditText txtAk;
    private EditText txtCv;
    private EditText txtQr;
    private EditText txtRes;

    private TextView display;

    private Double txt_ancien_kilometrage;
    private Double txt_conso_vehicule;
    private Double txt_qte_approv;
    private Double txt_nouveau_kilometrage;
    //private Common m_common ;

    public MainActivity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        txtAk =  (EditText) findViewById(R.id.txt_ancien_kilometrage);
        txtCv =  (EditText) findViewById(R.id.txt_conso_vehicule);
        txtQr =  (EditText) findViewById(R.id.txt_qte_approv);

        txtRes=  (EditText) findViewById(R.id.txt_resultat);
        display= (TextView) findViewById(R.id.display_primary);




        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                //Validate input
                if ("".equals(txtAk.getText().toString()) && "".equals(txtCv.getText().toString()) && "".equals(txtQr.getText().toString())) {
                    txtAk.setError("Veuillez saisir votre ancien kilometrage sur le reçu.");
                    txtCv.setError("Veuillez saisir la conso moyenne aux 100 du vehicule.");
                    txtQr.setError("Veuillez saisir la derniere qté de carburant prise.");
                    return;
                }

                if ("".equals(txtAk.getText().toString())) {
                    txtAk.setError("Veuillez saisir votre ancien kilometrage sur le reçu.");
                    return;
                }
                if ("".equals(txtCv.getText().toString())) {
                    txtCv.setError("Veuillez saisir la conso moyenne aux 100 du vehicule.");
                    return;
                }
                if ("".equals(txtQr.getText().toString())) {
                    txtQr.setError("Veuillez saisir la derniere qté de carburant prise.");
                    return;
                }

                txt_ancien_kilometrage = Double.parseDouble(txtAk.getText().toString());
                txt_conso_vehicule = Double.parseDouble(txtCv.getText().toString());
                txt_qte_approv = Double.parseDouble(txtQr.getText().toString());
                try {
                    txt_nouveau_kilometrage = EssenceMalo(txt_qte_approv, txt_conso_vehicule, txt_ancien_kilometrage);
                } catch (Exception e) {
                    txt_nouveau_kilometrage = 0.0;
                    e.printStackTrace();
                }

                txtRes.setText(txt_nouveau_kilometrage.toString());
                display.setText(txt_nouveau_kilometrage.toString());
                return;

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
